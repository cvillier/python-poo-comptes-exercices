"""
    La classe compte épargne
"""
from Compte import Compte
from Constantes import POURCENTAGE_INTERETS

class CompteEpargne(Compte):

    def appliquer_interets(self, montant:float):
        interet = (1+POURCENTAGE_INTERETS) * montant
        return interet


    def __str__(self):
        return "compte d'épargne n°: {0}, titulaire : {1} avec un solde de {2}€".format(self.numero_compte, self.nom_proprietaire, self.solde)
