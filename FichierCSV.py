"""
    Notre classe qui va gérer l'enregistrement du fichier CSV avec la liste des comptes
"""
import csv
import os


def creation_fichier_CSV():

    #nom_du_fichier = input('Nom de votre fichier (avec l extension SVP !) : ')
    try:
        with open("innovators.csv", 'w', encoding='utf-8') as file:
            writer = csv.writer(file)
            writer.writerow(["SN", "Name", "Contribution"])
            writer.writerow([1, "Linus Torvalds", "Linux Kernel"])
            writer.writerow([2, "Tim Berners-Lee", "World Wide Web"])
            writer.writerow([3, "Guido van Rossum", "Python Programming"])
    except Exception as e:
        print(e)


print(creation_fichier_CSV())