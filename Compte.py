"""
    Notre classe Compte
"""
from abc import ABC

class Compte(ABC):

    numero_compte = ''
    nom_proprietaire = ''
    solde = 0

    def retrait(self, montant:int ):
        self.solde -= montant

    def versement(self, montant:int ):
        self.solde += montant

    def afficher_solde(self):
        return self.solde

    def __init__(self, numero_compte, nom_proprietaire, solde):
        self.numero_compte = numero_compte
        self.nom_proprietaire = nom_proprietaire
        self.solde = solde