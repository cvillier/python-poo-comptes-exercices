"""
    La classe compte courant
"""
from Constantes import POURCENTAGE_AGIOS
from Compte import Compte


class CompteCourant(Compte):

    autorisation_decouvert = 0

    def appliquer_agios(montant:float):
        if montant < 0:
            agios_a_payer = montant * POURCENTAGE_AGIOS
            return abs(agios_a_payer)
            #print('Agios à payer : ', agios_a_payer, ' €')
        else:
            raise Exception('ERREUR : on ne calcul pas des Agios sur un compte au solde positif')



    def __str__(self):
        return "compte courant n°: {0}, titulaire : {1} avec un solde de {2}€ avec une autorisation de découvert de {3}€".format(self.numero_compte, self.nom_proprietaire, self.solde, self.autorisation_decouvert)