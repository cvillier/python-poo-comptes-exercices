"""
    La classe main pour lancer nos méthodes
"""
from Compte import *
from CompteCourant import *
from CompteEpargne import *


mon_compte_courant = CompteCourant('118218', 'Erwann Duclos', 100)
mon_compte_courant.autorisation_decouvert = -200.00

mon_compte_epargne = CompteEpargne('911', 'Corentin Villiermet', 2000)

def operation(compte_sur_lequel_travailler:Compte):

    """
        On demande le compte
    """
    compte_choisi = input('Choisir votre compte : C pour votre compte courant ou E pour votre compte d épargne : ')
    if compte_choisi != 'E' and compte_choisi != 'C':
        return Exception('Erreur : vous devez taper la lettre "C" pour les opérations concernant votre compte courant ou "E" pour votre compte d épargne, veuillez réessayer')


    """
        On demande le montant
    """
    montant_choisi = input('Quel montant souhaitez-vous virer ou retirer (précédé du signe "-") ? : ')
    montant_choisi = float(montant_choisi)
    #if not isinstance(montant_choisi, float):
     #   return Exception('Vous devez rentrer un nombre (avec jusqu à 2 chiffres après la virgule)') attention avec input -> str

    if montant_choisi > 2500.00 or montant_choisi < -2500.00:
        return Exception('La somme a manipuler est trop importante : veuillez contacter votre conseiller pour cette opération !!!')


    """
        On applique les opérations pour les comptes d'épargnes
    """
    if compte_choisi == 'E':

        if compte_sur_lequel_travailler.solde + montant_choisi < 0:
            return Exception('Vous ne pouvez pas vider votre compte dépargne')

        else:
            ancient_solde = compte_sur_lequel_travailler.afficher_solde()
            print('votre solde : ', ancient_solde, ' €')

            compte_sur_lequel_travailler.versement(montant_choisi)
            print('le montant de l opération ', montant_choisi, ' €')

            nouveau_solde = compte_sur_lequel_travailler.afficher_solde()
            print('nouveau solde : ', nouveau_solde, ' €')

            interets = round(compte_sur_lequel_travailler.appliquer_interets(nouveau_solde),2)
            print('montant des interets à la fin du mois : ', interets, ' €')


    """
        On applique les opérations pour les comptes courants
    """
    if compte_choisi == 'C':

        if compte_sur_lequel_travailler.solde + montant_choisi < compte_sur_lequel_travailler.autorisation_decouvert:
            print('Le retrait que vous demandez dépasse votre autorisation de prélévement')

        else:

            ancient_solde = compte_sur_lequel_travailler.afficher_solde()
            print('votre solde : ', ancient_solde, ' €')

            compte_sur_lequel_travailler.versement(montant_choisi)
            print('le montant de l opération ', montant_choisi, ' €')

            nouveau_solde = compte_sur_lequel_travailler.afficher_solde()
            print('nouveau solde : ', nouveau_solde, ' €')

            if compte_sur_lequel_travailler.solde + montant_choisi < 0:
                agios = CompteCourant.appliquer_agios(nouveau_solde)
                print('montant des agios à la fin du mois : ', agios, ' €')


    return [compte_choisi, montant_choisi]

print(mon_compte_epargne)
print(mon_compte_courant)

print(operation(mon_compte_epargne))

